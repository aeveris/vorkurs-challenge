#!/bin/bash

if ! which pandoc &> /dev/null; then
    echo "You need pandoc installed for this to work."
fi

pandoc -t html -o docs.html docs.md
