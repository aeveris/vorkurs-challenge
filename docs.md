# Livecoding Docs

You seem to seek the wisdom needed to not embarrass yourself on this fine day. These docs shall grant you the necessary knowledge to achieve this.

## The task

Your task is to get and decrypt the secret message. To do so you will need to issue some [requests](https://requests.readthedocs.io/en/master/) to the server at the `/api` endpoint, find the key and then decrypt the message.

## Talk to me!

The server seems to be in a bad mood, so you need to be especially nice and polite.
Maybe try to greet it with the following: "`X-Polite: action=tipping-fedora;greeting=m'lady`".

## Dark secrets

So now that we understand each other we can finally get down to business.

The message is **AES-CBC** encrypted and encoded as a **hexadecimal** string. The **first 16 bytes** of the ciphertext comprise the **IV**.
You need to convert the hexadecimal representation to bytes and **split** it into IV and message ciphertext.
The length of the key is **16 bytes/ASCII characters**.
Once decrypted you need to **unpad** the result (remember: the blocksize is 128 bits) before you can finally decode it to a string.

As always, it's dangerous to go alone - take [this](https://cryptography.io/en/latest/)!


## Everybody gets a key

The key is split into **two parts** in the JSON response. Each part is 8 characters lowercase ASCII and encoded in a list of strings.
To decode either half of the key you need to count each character's occurrence for each position and choose the ones with the highest frequency.
Finally, concatenate both parts and you have yourself a key. Nice!

### Key encoding example

The example key `secret` could be encoded like so:

````
sihrebxt
lzqfvvct
wmyrptsb
nevrejff
fabkhdsj
oacxjtfu
sbchtcjf
melqetzn
meciabry
sdkxpxfe
````

If you count the occurrences of the first letters in each string, you will see that the letter `s` can be found more often than any other one.

## Cake

Have some.