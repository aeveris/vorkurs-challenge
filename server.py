import random

from flask import Flask, render_template, request, jsonify

from cryptostuff import split_and_encode_key, Cryptor

KEY = 'deadbeefbeerbark'
ENC_KEY = split_and_encode_key(KEY)

APP = Flask('livecoding', template_folder='.')
SECRET_MESSAGE = '''
                                i.
                                .7.
                               .. :v
                              c:  .x
                               i.::
                                 :
                                ..i..
                               #MMMMM
                               QM  AM
                               9M  zM
                               6M  AM
                               2;  2;X#MM@1.
                               0M  tMMMMMMMMMM;
                          .X#MMMM  ;MMMMMMMMMMMMv
                      cEMMMMMMMMMU7@MMMMMMMMMMMMM@
                .n@MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
               MMMMMMMM@@#$BWWB#@@#$WWWQQQWWWWB#@MM.
               MM                                ;M.
               $M                                EM
               WMO$@@@@@@@@@@@@@@@@@@@@@@@@@@@@#OMM
               #M                                cM
               QM                                tM
               MM                                CMO
            .MMMM                                oMMMt
           1MO 6MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM iMM
          .M1  BM                                vM  ,Mt
          1M   @M .............................. WM   M6
           MM  .A8OQWWWWWWWWWWWWWWWWWWWWWWWWWWW0Az2  #M
            MM                                      MM.
             @MMY                                vMME
               UMMMbi                        i8MMMt
                  C@MMMMMbt;;i.......i;XQMMMMMMt
                       ;ZMMMMMMMMMMMMMMM@A;.

You made it, have yourself some cake. And a party: https://youtu.be/LDU_Txk06tM?t=75
And now go forth and learn a proper language: https://learnxinyminutes.com/docs/rust/
For science, you monster.
'''
CIPHERTEXT = Cryptor(KEY).encrypt(SECRET_MESSAGE)


@APP.route('/docs')
def docs():
    return render_template('docs.html')


@APP.route('/api', methods=['GET', 'POST'])
def api():
    try:
        greeting = request.headers['X-Polite']
        if greeting != 'action=tipping-fedora;greeting=m\'lady':
            raise ValueError
    except (KeyError, ValueError):
        return jsonify({'status': 'nope', 'message': 'How rude! How about a proper greeting?!'})
    if request.method == 'GET':
        answers = [
            'How rude! Maybe try to POSTpone your longing for things instead of trying to GET everything immediately?!',
            'How rude! Trying to GET your grubby hands on me! Adjust your manners POST haste!',
        ]
        return jsonify({'status': 'nuh-uh', 'message': random.choice(answers)})
    resp = {
        'status': 'halfway there',
        'keys': ENC_KEY,
        'ciphertext': CIPHERTEXT,
        'motivation': 'Well done, you did better than expected! On the other hand I wasn\'t expecting much in the first place.',
        'git': 'gud',
    }
    return jsonify(resp)


@APP.route('/')
def home():
    return 'Nothing here for you. Try "/api" or "/docs"'